# FastMatrix

[![pipeline status](https://gitlab.com/fralfaro/fastmatrix/badges/master/pipeline.svg)](https://gitlab.com/fralfaro/fastmatrix/-/commits/master)
[![coverage report](https://gitlab.com/fralfaro/fastmatrix/badges/master/coverage.svg)](https://gitlab.com/fralfaro/fastmatrix/-/commits/master)
<a href="https://fralfaro.gitlab.io/fastmatrix/"><img alt="Link a la Documentación" src="https://img.shields.io/badge/docs-link-brightgreen"></a>

<a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/static--analysis-black%20flake8%20mypy-black"></a>
<a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/testing-pytest-black"></a>
<a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/documentation-sphinx-black"></a>



Fast Computation of some Matrices Useful in Statistics







