# -*- coding: utf-8 -*-
import pytest
import numpy as np

from fastmatrix.operations.symmetric import (
    AsSymmetric,
    as_symmetric,
)


@pytest.mark.parametrize(
    "a,lower",
    [
        (np.zeros((2, 2)), False),
        (np.ones((2, 2)), True),
    ],
)
def test_assymmetric_correct(a, lower):
    AsSymmetric(a=a, lower=lower)


@pytest.mark.parametrize(
    "a,lower, expected",
    [
        (
            np.array(
                [
                    [1, 2, 3],
                    [2, 5, 6],
                    [3, 6, 9],
                ]
            ),
            False,
            np.array(
                [
                    [1, 2, 3],
                    [2, 5, 6],
                    [3, 6, 9],
                ]
            ),
        ),
        (
            np.array(
                [
                    [1, 5, 9, 13],
                    [2, 6, 10, 14],
                    [3, 7, 11, 15],
                    [4, 8, 12, 16],
                ]
            ),
            True,
            np.array(
                [[1, 2, 3, 4], [2, 6, 7, 8], [3, 7, 11, 12], [4, 8, 12, 16]]
            ),
        ),
        (
            np.array(
                [
                    [1, 5, 9, 13],
                    [2, 6, 10, 14],
                    [3, 7, 11, 15],
                    [4, 8, 12, 16],
                ]
            ),
            False,
            np.array(
                [
                    [1, 5, 9, 13],
                    [5, 6, 10, 14],
                    [9, 10, 11, 15],
                    [13, 14, 15, 16],
                ]
            ),
        ),
    ],
)
def test_compute(a, lower, expected):
    result_01 = AsSymmetric(a=a, lower=lower).compute()
    result_02 = as_symmetric(a, lower)
    assert np.array_equal(result_01, expected)
    assert np.array_equal(result_02, expected)
