Introduction
=============

Description
-----------

Small set of functions to fast computation of some matrices and operations
useful in statistics and econometrics. Currently, there are functions for efficient
computation of duplication, commutation and symmetrizer matrices with minimal storage
requirements. Some commonly used matrix decompositions (LU and LDL), basic matrix
operations (for instance, Hadamard, Kronecker products and the Sherman-Morrison formula)
and iterative solvers for linear systems are also available. In addition, the package
includes a number of common statistical procedures such as the sweep operator, weighted
mean and covariance matrix using an online algorithm, linear regression (using Cholesky,
QR, SVD, sweep operator and conjugate gradients methods), ridge regression (with optimal
selection of the ridge parameter considering the GCV procedure), functions to compute
the multivariate skewness, kurtosis, Mahalanobis distance (checking the positive
defineteness) and the Wilson-Hilferty transformation of chi squared variables.

.. note::
    The original projects was developed by `Felipe Osorio <http://fosorios.mat.utfsm.cl/software.html>`_.
    These repositories are only  a packages transform from R to Python.
    Many this functions are an extension from `Numpy <https://numpy.org/>`_ and
    `Scipy <https://www.scipy.org/>`_ packages.
