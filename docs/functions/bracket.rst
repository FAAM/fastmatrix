Bracket product
================

.. py:function:: bracket_prod(a:np.ndarray,x:np.ndarray)-> np.ndarray
.. automodule:: fastmatrix.operations.bracket.bracket_prod