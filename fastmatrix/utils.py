# -*- coding: utf-8 -*-
import numpy as np
from pydantic import BaseModel, validator


class SquareMatrix(BaseModel):
    """
    Class that define a square matrix

    :param a: two dimensional square matrix
    """

    a: np.ndarray

    class Config:
        arbitrary_types_allowed = True

    @validator("a")
    def dims_validation_a(cls, a):
        shape_a = a.shape
        assert len(shape_a) == 2, "matrix 'A' not is a two dimentional matrix"
        return a

    @validator("a")
    def square_validation(cls, a):
        shape_a = a.shape
        assert shape_a[0] == shape_a[1], "matrix 'A' not is a square matrix"

        return a


class SymmetricMatrix(SquareMatrix):
    """
    Class that define a symmetric matrix

    :param a: two dimensional symmetric matrix
    """

    @validator("a")
    def symmetric_validation(cls, a):
        assert check_symmetric(a), "matrix 'A' not is a symmetric matrix"
        return a


class PositiveDefiniteMatrix(SymmetricMatrix):
    """
    Class that define a positive definite matrix

    :param a: two dimensional positive definite matrix
    """

    @validator("a")
    def positive_definite_validation(cls, a):
        assert positive_definite(
            a
        ), "matrix 'A' not is a positive definite matrix"
        return a


def check_symmetric(
    a: np.ndarray, rtol: float = 1e-05, atol: float = 1e-08
) -> bool:
    """
    Check if a matrix is a symmetric matrix, i.e., :math:`A = A^t`

    :param a: square two dimentional matrix
    :param rtol: the relative tolerance parameter
    :param atol: the absolute tolerance parameter
    :return: True if the matrix is symmetric; False otherwise
    """

    return np.allclose(a, a.T, rtol=rtol, atol=atol)


def positive_definite(a: np.ndarray) -> np.bool_:
    """
    Check if a matrix is positive definite, i.e.,
    :math:`x^tMx >0,  \\forall x \\in \\mathbb{R}^n/\\0\\}`

    :param a: square two dimentional matrix
    :return: True if the matrix is positive definite; False otherwise
    """
    return np.all(np.linalg.eigvals(a) > 0)
