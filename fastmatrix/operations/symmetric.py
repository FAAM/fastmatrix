# -*- coding: utf-8 -*-
import numpy as np

from fastmatrix.utils import check_symmetric, SquareMatrix


class AsSymmetric(SquareMatrix):
    """
    Class that define validations for `as_symmetric` function
    """

    lower: bool = True

    def compute(self) -> np.ndarray:

        a = self.a

        if check_symmetric(a):
            return a
        else:
            X = np.tril(a) if self.lower else np.triu(a)
            X = X + X.T - np.diag(np.diag(X))
            return X


def as_symmetric(a: np.ndarray, lower: bool = True) -> np.ndarray:
    """
    Description
    -----------
    Force a square matrix to be symmetric


    Arguments
    ---------
    :param a: square matrix to be forced to be symmetric
    :param lower: logical, should the upper (lower)
                triangle be replaced with the lower (upper) triangle?
    :return: square symmetric matrix

    Examples
    --------

    .. code:: python

        from fastmatrix import check_symmetric,as_symmetric
        import numpy as np

        a = np.array([
            [1,5,9,13],
            [2,6,10,14],
            [3,7,11,15],
            [4,8,12,16],
        ])
        check_symmetric(a)

    ::

        False

    .. code:: python

        b = as_symmetric(a,lower = False)
        b

    ::

        array([[ 1,  5,  9, 13],
               [ 5,  6, 10, 14],
               [ 9, 10, 11, 15],
               [13, 14, 15, 16]])

    .. code:: python

        check_symmetric(b)

    ::

        True

    """

    return AsSymmetric(a=a, lower=lower).compute()
