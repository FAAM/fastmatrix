# -*- coding: utf-8 -*-

"""Top-level package for fastmatrix."""

__author__ = """Francisco Alfaro"""
__email__ = "francisco.alfaro.496@gmail.com"
__version__ = "0.1.0"


from fastmatrix.operations.array_multiplication import (
    array_mult,
)  # pyflakes.ignore
from fastmatrix.operations.symmetric import (
    as_symmetric,
)  # pyflakes.ignore
from fastmatrix.operations.bracket import bracket_prod  # pyflakes.ignore
from fastmatrix.linear_systems.cg import cg_fm  # pyflakes.ignore
from fastmatrix.utils import (
    check_symmetric,
    positive_definite,
)  # pyflakes.ignore
